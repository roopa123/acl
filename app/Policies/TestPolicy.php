<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use \App\User;
use \App\Posts;

class TestPolicy
{
    public function UpdatePost(User $user, Posts $post)
    {
        return $user->id == $post->user_id;
    }
}
