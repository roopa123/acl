<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
     protected $table = 'Posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'description'];
}
